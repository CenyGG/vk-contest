package com.example.android.vkposter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextInputEditText textView;
    ConstraintLayout constraintWithText;
    ConstraintLayout backgroundLayout;
    RadioGroup radioGroup;
    ConstraintLayout rootView;
    View tabIndicatorView;
    View tabMockView;
    View radioTab1;
    View radioTab2;
    LinearLayout footer;
    AppBarLayout header;

    int displayWidth;
    int displayHeight;
    float historyScale;

    List<PicturesOnBoard> itemsOnBoard = new ArrayList<>();
    boolean isStory = false;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        displayWidth = size.x;
        displayHeight = size.y;


        rootView = findViewById(R.id.main_content);
        textView = findViewById(R.id.section_label);
        constraintWithText = findViewById(R.id.layout_with_text);
        backgroundLayout = findViewById(R.id.layout_for_background);
        radioGroup = findViewById(R.id.radio_group);
        header = findViewById(R.id.appbar);
        footer = findViewById(R.id.linear_layout_sent);

        tabIndicatorView = findViewById(R.id.tab_indicator);
        tabMockView = findViewById(R.id.tab_mock);

        radioTab1 = findViewById(R.id.radio_tab1);
        radioTab2 = findViewById(R.id.radio_tab2);

        imageView= findViewById(R.id.imageView);

        radioTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animCheck1();
                noteClick();
            }
        });
        radioTab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animCheck2();
                storyClick();
            }
        });

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            itemsOnBoard.clear();
            // checkedId is the RadioButton selected
            switch (checkedId) {
                case R.id.white_radio:
                    backgroundLayout.setBackgroundColor(Color.WHITE);
                    break;
                case R.id.blue_radio:
                    GradientDrawable drawable = (GradientDrawable) getResources().getDrawable(R.drawable.blue_normal);
                    drawable.setCornerRadii(new float[]{0, 0, 0, 0, 0, 0, 0, 0});
                    backgroundLayout.setBackground(drawable);
                    break;
                case R.id.green_radio:
                    backgroundLayout.setBackground(getResources().getDrawable(R.drawable.green_normal));
                    break;
                case R.id.orange_radio:
                    backgroundLayout.setBackground(getResources().getDrawable(R.drawable.orange_normal));
                    break;
                case R.id.red_radio:
                    backgroundLayout.setBackground(getResources().getDrawable(R.drawable.red_normal));
                    break;
                case R.id.purpl_radio:
                    backgroundLayout.setBackground(getResources().getDrawable(R.drawable.purpl_normal));
                    break;
                case R.id.palm_radio:
                    Bitmap bottom = BitmapFactory.decodeResource(getResources(), R.drawable.bg_beach_bottom);
                    Bitmap center = BitmapFactory.decodeResource(getResources(), R.drawable.bg_beach_center);
                    Bitmap top = BitmapFactory.decodeResource(getResources(), R.drawable.bg_beach_top);
                    itemsOnBoard.add(new PicturesOnBoard(center,0,0,0,true));
                    itemsOnBoard.add(new PicturesOnBoard(bottom,0,displayHeight-bottom.getHeight()));
                    itemsOnBoard.add(new PicturesOnBoard(top,0,0));

                    int height;
                    int width;
                    if(!isStory){
                         height = backgroundLayout.getHeight();
                         width = backgroundLayout.getWidth();
                    }else{
                        height = displayHeight;
                        width = displayWidth;
                    }


                    double bottomToScale = (double) bottom.getWidth() / (double) width;
                    double centerToScale = (double) center.getWidth() / (double) width;
                    double topToScale = (double) top.getWidth() / (double) width;
                    Bitmap bottomScaled = Bitmap.createScaledBitmap(bottom, width, (int) (bottom.getHeight() / bottomToScale), false);
                    Bitmap centerScaled = Bitmap.createScaledBitmap(center, width, height, false);
                    Bitmap topScaled = Bitmap.createScaledBitmap(top, width, (int) (top.getHeight() / topToScale), false);

                    Bitmap compiled = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(compiled);
                    canvas.drawBitmap(centerScaled, 0, 0, null);
                    canvas.drawBitmap(topScaled, 0, 0, null);
                    canvas.drawBitmap(bottomScaled, 0, height - bottomScaled.getHeight(), null);

                    backgroundLayout.setBackground(new BitmapDrawable(getResources(), compiled));
                    backgroundLayout.setScaleY((float)height/(float)width);
                    backgroundLayout.setScaleX(1);
                    break;
                case R.id.stars_radio:
                    backgroundLayout.setBackground(getResources().getDrawable(R.drawable.bg_stars_center));
                    break;
                case R.id.plus_radio:
                    // switch to fragment 1
                    break;
            }
        });

    }

    private void animCheck1() {
        if (radioTab1.getX() + 1 < tabIndicatorView.getX()) {
            TranslateAnimation moveLeftToRight = new TranslateAnimation(radioTab1.getWidth(), 0, 0, 0);
            moveLeftToRight.setDuration(250);
            tabIndicatorView.startAnimation(moveLeftToRight);
            tabIndicatorView.setTranslationX(0);

        }
    }

    private void animCheck2() {
        if (radioTab2.getX() - 1 > tabIndicatorView.getX()) {
            TranslateAnimation moveLeftToRight = new TranslateAnimation(-radioTab2.getWidth(), 0, 0, 0);
            moveLeftToRight.setDuration(250);

            tabIndicatorView.startAnimation(moveLeftToRight);
            tabIndicatorView.setTranslationX(radioTab2.getWidth());

        }
    }

    private void storyClick() {
        isStory = true;
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        header.setAlpha(0.8f);
        footer.setAlpha(0.8f);
        float scaleStory = (float) displayHeight / (float) displayWidth;
        historyScale = scaleStory;

//        Bitmap compiled = Bitmap.createBitmap(displayWidth, displayHeight, Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(compiled);
//        if(!itemsOnBoard.isEmpty()){
//            for(PicturesOnBoard item : itemsOnBoard){
//                if(item.isBackground){
//                    Bitmap sacled = Bitmap.createScaledBitmap(item.img, displayWidth, displayHeight, false);
//                    canvas.drawBitmap(sacled, item.x, item.y, null);
//                }else{
//                    Bitmap sacled = Bitmap.createScaledBitmap(item.img, displayWidth, (int)((float)displayWidth/(float)item.img.getWidth() * (float)displayHeight), false);
//                    canvas.drawBitmap(sacled, item.x,  (int)((float)item.img.getWidth()/(float)displayWidth *item.y), null);
//                }
//
//            }
            imageView
                    .animate()
                    .scaleX(scaleStory)
                    .scaleY(scaleStory)
                    .withEndAction(() -> {
                        imageView.setScaleY(scaleStory);
                        imageView.setBackground(getResources().getDrawable(R.drawable.beach_bg));
                    }).start();
//        }else{
//            backgroundLayout
//                    .animate()
//                    .scaleX(scaleStory)
//                    .scaleY(scaleStory)
//                    .withEndAction(() -> {
//                        backgroundLayout.setScaleX(scaleStory);
//                        backgroundLayout.setScaleY(scaleStory);
//                    }).start();
//        }


    }

    private void noteClick() {
        isStory = false;
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        header.setAlpha(1.0f);
        footer.setAlpha(1.0f);

        if (backgroundLayout.getBackground() != null) {
        float scaleStory = (float) displayWidth / (float) displayHeight;

//        iamgeView.setScaleType(ImageView.ScaleType.MATRIX);
//        Matrix m = new Matrix();
//        m.postScale(scaleStory, scaleStory);

        backgroundLayout
                .animate()
                .scaleX(1)
                .scaleY(1)
                .withEndAction(() -> {
                    backgroundLayout.setScaleY(1);
                    backgroundLayout.setScaleX(1);
//                    iamgeView.setImageMatrix(m);
//                    iamgeView.setScaleX(scaleStory);
//                    iamgeView.setScaleY(scaleStory);
                }).start();
        }


    }

    private void animUnscale() {

    }

    class PicturesOnBoard{
        public PicturesOnBoard(Bitmap img, float x, float y) {
            this.img = img;
            this.x = x;
            this.y = y;
            this.rot = 0;
            this.isBackground = false;
        }

        public PicturesOnBoard(Bitmap img, float x, float y, float rot, boolean isBackground) {
            this.img = img;
            this.x = x;
            this.y = y;
            this.rot = rot;
            this.isBackground = isBackground;
        }

        Bitmap img;
        float x;
        float y;
        float rot;
        boolean isBackground;
    }

}
